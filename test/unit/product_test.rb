require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  test "check create" do
    product = Product.new(:category_id => 1, :description => "testartid",
                          :name => "testname", :price => 1234, :user_id => 1,
                          :weight => 3242)
    assert_equal product.save, true
  end
  
  test "check create without category_id" do
    product = Product.new(:description => "testartid",
                          :name => "testname", :price => 1234, :user_id => 1,
                          :weight => 3242)
    assert_equal product.save, false
  end
  
  test "check create without description" do
    product = Product.new(:category_id => 1,
                          :name => "testname", :price => 1234, :user_id => 1,
                          :weight => 3242)
    assert_equal product.save, false
  end
  
  test "check create with bad description length" do
    product = Product.new(:category_id => 1, :description => "a",
                          :name => "testname", :price => 1234, :user_id => 1,
                          :weight => 3242)
    assert_equal product.save, false
  end
  
  test "check create without name" do
    product = Product.new(:category_id => 1, :description => "testartid",
                          :price => 1234, :user_id => 1,
                          :weight => 3242)
    assert_equal product.save, false
  end
  
  test "check create without price" do
    product = Product.new(:category_id => 1, :description => "testartid",
                          :name => "testname", :user_id => 1,
                          :weight => 3242)
    assert_equal product.save, false
  end
  
  test "check create with price non numeric" do
    product = Product.new(:category_id => 1, :description => "testartid",
                          :name => "testname", :price => "abfd", :user_id => 1,
                          :weight => 3242)
    assert_equal product.save, false
  end
  
  test "check create without user_id" do
    product = Product.new(:category_id => 1, :description => "testartid",
                          :name => "testname", :price => 1234,
                          :weight => 3242)
    assert_equal product.save, false
  end
  
  test "check create without weight" do
    product = Product.new(:category_id => 1, :description => "testartid",
                          :name => "testname", :price => 1234, :user_id => 1)
    assert_equal product.save, false
  end
  
  test "check create with weight non numeric" do
    product = Product.new(:category_id => 1, :description => "testartid",
                          :name => "testname", :price => 1234, :user_id => 1,
                          :weight => "abcd")
    assert_equal product.save, false
  end
 
end
