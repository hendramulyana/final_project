require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  test "check create without name" do
    category = Category.new(:parent_id => 1234)
    assert_equal category.save, false
  end
  
  test "check create with bad length name" do
    category = Category.new(:name => "testnjkfhasdkfhdfhasdkghsdkghsdjkghksdhgkjsdhgjkshgkjsdhgjksdhgksdhgshadgjkshgjshgjkshdgjhsdjghsdjkghsdjkhjfhdklhslfkhsdfdlskfkjdklfjklhasdjkfhdjklsahflkjdshfksdjhfkjdhsfkjadhsfkjdhaskjfdhaskjfhdsjkahfjkslhldjkahfljksdhfkjsdhfljkdsahfjkdahsfkjadhfjkdhasfjkdhaskfjhdskajfhasdkfjhdksfjhasjkfhdjkashfkashfkjadshfkjasdhfkjdhasjkfhasdkjfhasdkjfhasdkjfhkajsdfhdjkasfhame", :parent_id => 1234)
    assert_equal category.save, false
  end
  
  test "check create not unique name" do
    category = Category.create(:name => "testname", :parent_id => 1234)
    category = Category.new(:name => "testname", :parent_id => 1234)
    assert_equal category.save, false
  end
  
  test "check create without parent_id" do
    category = Category.new(:name => "testname")
    assert_equal category.save, false
  end
  
  test "check create" do
    category = Category.new(:name => "testname", :parent_id => 1234)
    assert_equal category.save, true
  end
  
  test "check get parent name" do
    category = Category.find(categories(:one))
    assert_equal category.parent_name, ""
  end
   
  test "check get child" do
    assert_not_nil Category.get_child(1)
  end
  
  test "check get parent" do
    assert_not_nil Category.get_parent
  end

end
