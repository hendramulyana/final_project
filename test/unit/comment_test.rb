require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  test "check create without comment" do
    comment = Comment.new(:article_id => "testartid", :user_id => 1)
    assert_equal comment.save, false
  end
  
  test "check create with bad comment length" do
    comment = Comment.new(:comment => "testcjfhasdkfhasdhfshfksdhfjkhdsfkjhsdjlfsdlahkfjhdlsakjfhlsdkfhlksdfhlksdahfklsdahflkjdashfjkdhsfklhdasklfhasdklfhldksjfhlsdjkfhksdjahfkljashfkljshdfjklshafjklhakljfhdkljfhsdjkafhdkjashfkljdashfkdhskflhsdlkfjhdjkfhsjkladhflsjkdahfljkashfljkdashfjkasdhfomment", :article_id => "testartid", :user_id => 1)
    assert_equal comment.save, false
  end
  
  test "check create without article_id" do
    comment = Comment.new(:comment => "testcomment", :user_id => 1)
    assert_equal comment.save, false
  end
  
  test "check create without user_id" do
    comment = Comment.new(:comment => "testcomment", :article_id => "testartid")
    assert_equal comment.save, false
  end
  
  test "check create" do
    comment = Comment.new(:comment => "testcomment", :article_id => "testartid", :user_id => 1)
    assert_equal comment.save, true
  end
  
end
