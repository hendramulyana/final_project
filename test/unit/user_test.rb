require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "check create" do
    user = User.new(:password => "testpass", :password_confirmation => "testpass", :address => "testaddress", 
                            :birthday => "10-10-2010", :blog => "testblog", :email => "tset@email.com", 
                            :facebook => "test@facebook.com", :gender => true, :name => "testname", 
                            :phone_number => "123524")
    assert_equal user.save, true
  end

  test "check create without password" do
    user = User.new(:password_confirmation => "testpass", :address => "testaddress", 
                            :birthday => "10-10-2010", :blog => "testblog", :email => "tset@email.com", 
                            :facebook => "test@facebook.com", :gender => true, :name => "testname", 
                            :phone_number => "123524")
    assert_equal user.save, false
  end

  test "check create with bad password length" do
    user = User.new(:password => "a", :password_confirmation => "testpass", :address => "testaddress", 
                            :birthday => "10-10-2010", :blog => "testblog", :email => "tset@email.com", 
                            :facebook => "test@facebook.com", :gender => true, :name => "testname", 
                            :phone_number => "123524")
    assert_equal user.save, false
  end

  test "check create without email" do
    user = User.new(:password => "testpass", :password_confirmation => "testpass", :address => "testaddress", 
                            :birthday => "10-10-2010", :blog => "testblog", 
                            :facebook => "test@facebook.com", :gender => true, :name => "testname", 
                            :phone_number => "123524")
    assert_equal user.save, false
  end
  
  test "check create with bad email format" do
    user = User.new(:password => "testpass", :password_confirmation => "testpass", :address => "testaddress", 
                            :birthday => "10-10-2010", :blog => "testblog", :email => "tfdmailcom", 
                            :facebook => "test@facebook.com", :gender => true, :name => "testname", 
                            :phone_number => "123524")
    assert_equal user.save, false
  end
  
  test "check is admin" do
    assert_equal User.find(users(:one)).is_admin?, false
  end
  
end
