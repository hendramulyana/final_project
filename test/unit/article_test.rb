require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  test "check create without body" do
    article = Article.new(:title => "testtitle", :user_id => 1)
    assert_equal article.save, false
  end
  
  test "check create with bad length body" do
    article = Article.new(:title => "testtitle", :body => "a", :user_id => 1)
    assert_equal article.save, false
  end
  
  test "check create without title" do
    article = Article.new(:body => "testbodyy", :user_id => 1)
    assert_equal article.save, false
  end
  
  test "check create with bad length title" do
    article = Article.new(:title => "testtitlefsdhkfhsdfkhsdkjghsdkjhgkdjsahgdjkshgdjksghdkjsaghsdjkaghsjkghdsjkhgkjhgjkdshgkdjshgkjsdhgkjhdsjkghsdjkghdsjkghsdkjghsdkjaghjksaghdksjahgkjsdhgdjkshgkjsdahgkjasdhgkjasdhgjksdhgjksahgjksdhagkjasdhgkjadhsgkjasdhgkjhdasjkghsdakjghasdgjkhasdjkghsdjkghasdjkghasdkjghasdjkghasdjkghasdjkghsdjkghasdjkghasjkhgajkhgjkdshgjksdhgkjsdhagjkhsdgjkhsdjakghasjkghasdhgjkasdhgjaksdhgjksdhgjkasdhgjkshdgjkshgjksdhgjkhsdjgkhadsjkghdasjkghasdjkghasdjkghsdjghasdkjghadsjkghasdkgjhaksjg",
                          :body => "testbodyy", :user_id => 1)
    assert_equal article.save, false
  end
  
  test "check create without user_id" do
    article = Article.new(:title => "testtitle", :body => "testbodyy")
    assert_equal article.save, false
  end
  
  test "check create" do
    article = Article.new(:title => "testlength",
                          :body => "testbodyy", :user_id => 1)
    assert_equal article.save, true
  end
  
end
