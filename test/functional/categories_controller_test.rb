require 'test_helper'

class CategoriesControllerTest < ActionController::TestCase
  test "should get index" do
    login_as(users(:one).email)
    get :index
    assert_redirected_to root_path
  end
  
  test "should get admin category index" do
    login_as(users(:two).email)
    get :index
    assert_redirected_to admin_categories_path
  end
 
end
