require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
    @user = users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user" do
    post :create, user: {email: "great@test.com", password: "testpass", password_confirmation: "testpass"}
    assert_redirected_to root_path
  end

  test "should create user bad param" do
    post :create, user: {email: "bademail", password: "testpass", password_confirmation: "testpass"}
    assert_response :success
  end

  test "should create user bad password" do
    post :create, user: {email: "great@test.com", password: "tesgags", password_confirmation: "testpass"}
    assert_response :success
  end

  test "should show user" do
    login_as(@user.email)
    get :show, id: User.find(@user).id
    assert_response :success
  end

  test "should get edit" do
    login_as(@user.email)
    get :edit, id: @user
    assert_response :success
  end

  test "should update user bad password" do
    login_as(@user.email)
    post :update, id: User.find(@user).id, user: {email: "test@test.com", password: "tesgsfadgags", password_confirmation: "testpass"}
    assert_response :success
  end

  test "should update user good password" do
    login_as(@user.email)
    post :update, id: User.find(@user).id, user: {email: "test@test.com", password: "testpass", password_confirmation: "testpass"}
    assert_response :redirect
  end

  test "should update user" do
    login_as(@user.email)
    post :update, id: User.find(@user).id, user: { address: @user.address }
    assert_redirected_to assigns(:user)
  end
  
  test "should update user bad ownership" do
    login_as(@user.email)
    post :update, id: User.find(users(:two)).id, user: { address: @user.address, email: "bademail"}
    assert_redirected_to root_path
  end
  
  test "should update user bad param" do
    login_as(@user.email)
    post :update, id: User.find(users(:one)).id, user: { address: @user.address, email: "bademail"}
    assert_response :success
  end
end
