require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:products)
  end
  
  test "should get index with filter" do
    get :index, f: Category.find(categories(:one)).id
    assert_response :success
    assert_not_nil assigns(:products)
  end

  test "should get my products" do
    login_as(users(:one).email)
    get :my_products
    assert_response :success
  end


  test "should get new" do
    login_as(users(:one).email)
    get :new
    assert_response :success
  end

  test "should create product" do
    login_as(users(:one).email)
    assert_difference('Product.count') do
      post :create, product: { category_id: @product.category_id, description: @product.description, name: @product.name, price: @product.price, user_id: @product.user_id, weight: @product.weight }
    end

    assert_redirected_to my_products_path
  end
  
  test "should create product bad param" do
    login_as(users(:one).email)
    post :create, product: { category_id: @product.category_id, description: @product.description, price: @product.price, user_id: @product.user_id, weight: @product.weight }
    assert_response :success
  end

  test "should show product" do
    login_as(users(:one).email)
    product = Product.find(products(:one))
    product.update_attributes(:user_id => User.find(users(:one)).id, :category_id => Category.find(categories(:one)).id)
   
    get :show, id: product
    assert_response :success
  end

  test "should get edit" do
    login_as(users(:one).email)
    product = Product.find(products(:one))
    product.update_attribute(:user_id, User.find(users(:one)).id)
    
    get :edit, id: product
    assert_response :success
  end

  test "should update product bad ownership" do
    login_as(users(:one).email)
    product = Product.find(products(:one))
    
    put :update, id: product, product: {description: @product.description}
    assert_redirected_to root_path
  end

  test "should update product" do
    login_as(users(:one).email)
    product = Product.find(products(:one))
    product.update_attribute(:user_id, User.find(users(:one)).id)
    
    put :update, id: product, product: {description: @product.description}
    assert_redirected_to my_products_path
  end

  test "should update product bad param" do
    login_as(users(:one).email)
    product = Product.find(products(:one))
    product.update_attribute(:user_id, User.find(users(:one)).id)
    
    put :update, id: product, product: {description: "", name: ""}
    assert_response :success
  end

  test "should destroy product" do
    login_as(users(:one).email)
    product = Product.find(products(:one))
    product.update_attribute(:user_id, User.find(users(:one)).id)
    
    assert_difference('Product.count', -1) do
      delete :destroy, id: product
    end

    assert_redirected_to my_products_path
  end
end
