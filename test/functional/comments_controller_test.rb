require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  test "should create bad param" do
    login_as(users(:two).email)
    post :create, comment: { comment: "testugsgsgpdate", article_id: Article.find(articles(:one)).id}
    assert_response :redirect
  end
  
  test "should create" do
    login_as(users(:two).email)
    post :create, comment: { comment: "tgsxgsestufdfpdate", article_id: Article.find(articles(:one)).id, user_id: User.find(users(:two)).id}
    assert_response :redirect
  end
end
