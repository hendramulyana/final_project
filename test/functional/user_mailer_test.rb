require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test "should send mail to user" do
    assert_not_nil UserMailer.registration_confirmation(User.find(users(:one)))
  end
end
