require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  test "should get create as admin" do
    post :create, password: "1234", username: "admin@admin.com"
    assert_response :redirect
  end
  
  test "should get create as user" do
    post :create, password: "1234", username: "hendra.mulyana@kiranatama.com"
    assert_response :redirect
  end
  
  test "should get create bad login" do
    post :create, password: "123432", username: "hendra.mulyana@kiranatama.com"
    assert_response :redirect
  end
  
  test "should get destroy" do
    login_as("admin@admin.com")
    delete :destroy
    assert_response :redirect
  end
end
