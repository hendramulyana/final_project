require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  setup do
    @article = articles(:one)
  end

  test "should get my articles without login" do
    get :my_articles
    assert_response :redirect
  end

  test "should get my articles" do
    login_as(users(:one).email)
    get :my_articles
    assert_response :success
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:articles)
  end

  test "should get new" do
    login_as(users(:one).email)
    
    get :new
    assert_response :success
  end

  test "should create article" do
    login_as(users(:one).email)
    
    assert_difference('Article.count') do
      post :create, article: { body: @article.body, title: @article.title, user_id: @article.user_id }
    end

    assert_redirected_to my_articles_path
  end

  test "should create article bad param" do
    login_as(users(:one).email)
    post :create, article: { body: "", title: "", user_id: @article.user_id }

    assert_response :success
  end

  test "should edit article bad ownership" do
    login_as(users(:one).email)
    article = Article.find(articles(:one))
    article.update_attribute(:user_id, User.find(users(:two)).id)
    
    get :edit, id: article
    assert_response :redirect
  end

  test "should show article" do
    login_as(users(:one).email)
    article = Article.find(articles(:one))
    article.update_attribute(:user_id, User.find(users(:one)).id)
    
    get :show, id: article
    assert_response :success
  end

  test "should get edit" do
    login_as(users(:one).email)
    article = Article.find(articles(:one))
    article.update_attribute(:user_id, User.find(users(:one)).id)
    
    get :edit, id: article
    assert_response :success
  end

  test "should update article" do
    login_as(users(:one).email)
    article = Article.find(articles(:one))
    article.update_attribute(:user_id, User.find(users(:one)).id)
    
    put :update, id: article, article: { body: @article.body, title: @article.title, user_id: @article.user_id }
    assert_redirected_to article_path(assigns(:article))
  end
  
  test "should update article bad param" do
    login_as(users(:one).email)
    article = Article.find(articles(:one))
    article.update_attribute(:user_id, User.find(users(:one)).id)
    
    put :update, id: article, article: { body: @article.body, title: "", user_id: @article.user_id }
    assert_response :success
  end

  test "should destroy article" do
    login_as(users(:one).email)
    article = Article.find(articles(:one))
    article.update_attribute(:user_id, User.find(users(:one)).id)
    
    assert_difference('Article.count', -1) do
      delete :destroy, id: article
    end

    assert_redirected_to my_articles_path
  end
end
