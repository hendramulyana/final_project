require 'test_helper'

class Admin::CategoriesControllerTest < ActionController::TestCase
  test "should get index" do
    login_as(users(:two).email)
    get :index
    assert_response 200
  end
  
  test "should get show" do
    login_as(users(:two).email)
    get :show, id: Category.find(categories(:two)).id
    assert_response 200
  end
  
  test "should get show without login" do
    get :show, id: Category.find(categories(:two)).id
    assert_response :redirect
  end
  
  test "should get new" do
    login_as(users(:two).email)
    get :new
    assert_response :success
  end
  
  test "should get edit" do
    login_as(users(:two).email)
    get :edit, id: Category.find(categories(:two)).id
    assert_response :success
  end
  
  test "should get destroy" do
    login_as(users(:two).email)
    delete :destroy, id: Category.find(categories(:two)).id
    assert_response :redirect
  end
  
  test "should update" do
    login_as(users(:two).email)
    put :update, id: Category.find(categories(:two)).id, category: { name: "testupdate"}
    assert_response :redirect
  end
  
  test "should update bad param" do
    login_as(users(:two).email)
    put :update, id: Category.find(categories(:two)).id, user: { name: "testupdate"}
    assert_response 200
  end
  
  test "should create" do
    login_as(users(:two).email)
    put :create, id: Category.find(categories(:two)).id, category: { name: "testupdate", parent_id: 1}
    assert_response :redirect
  end
  
  test "should create bad param" do
    login_as(users(:two).email)
    put :create, id: Category.find(categories(:two)).id, user: { name: "testupdate", parent_id: 1}
    assert_response 200
  end
end
