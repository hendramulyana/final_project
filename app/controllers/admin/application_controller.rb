class Admin::ApplicationController < ApplicationController
  protect_from_forgery
  
  #check is user admin
  def require_admin_login
    if current_user.nil? || !current_user.is_admin?
      flash[:notice] = "You don't have previllage to access this page"
      redirect_to root_path
    end
  end
  
end
