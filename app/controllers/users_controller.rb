class UsersController < ApplicationController
  before_filter :check_self_user, :only => [:edit, :update, :show]
  
  #prevent user editing other user 
  def check_self_user
    if Integer(params[:id]) != session[:user_id]
      flash[:session] = "You are not authorized"
      redirect_to root_path
    end
  end
  
  # GET /users
  # GET /users.json
  def index
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])
    
      #check only recaptcha in web client
      if params[:mobile].nil? 
        unless verify_recaptcha
          @user.errors[:recaptcha] << "String you entered didn't match the recaptcha"
          render :action => :new, :format => :html
          return
        end
      end
      
      if params[:user][:password] != params[:user][:password_confirmation]
        @user.errors[:password] << "Password and Confirmation did not match"
        render :action => :new, :format => :html
        return
      end
      
      respond_to do |format|
      if @user.save
        session[:user_id] = User.find(@user).id
        UserMailer.registration_confirmation(@user).deliver
        format.html { redirect_to edit_user_path(@user), flash[:session] => 'Registration Complete' }
        format.json { render json: "Registration Complete", status: :created, location: @user }
      else
        @user.errors[:email] = "Registration Failed"
        format.html { render :action => "new"}
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      
      #change password
      unless params[:user][:password].nil?
        if params[:user][:password] != params[:user][:password_confirmation]
          @user.errors[:password] = "Password and confirmation didn't match"
          render action: "edit"
          format.json { render json: @user.errors, status: :unprocessable_entity }
          return
        end
        @user.encrypt_password
      end
      
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
end
