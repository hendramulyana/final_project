class SessionsController < ApplicationController
  #user login mechanism
  def create
    user = User.authenticate(params[:username], params[:password])
    if user
      session[:user_id] = user.id
      if user.is_admin?
        redirect_to admin_categories_path
      else
        redirect_to edit_user_path(session[:user_id])
      end
    else
      flash[:session] = "Invalid username or password" 
      redirect_to root_path
    end
  end
  
  #user logout mechanism
  def destroy
    session[:user_id] = nil
    redirect_to root_url, :notice => "Logged out!"
  end
end
