class CategoriesController < ApplicationController
  before_filter :check_admin, :on => :index
  
  #check is user an admin, if admin redirect to admin namespace
  def check_admin
    if current_user.is_admin?
      redirect_to admin_categories_path
    else
      flash[:session] = "Forbidden access"
      redirect_to root_path
    end
  end
  
  def index; end
  
end
