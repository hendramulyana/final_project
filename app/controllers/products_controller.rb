class ProductsController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]
  before_filter :check_self_owner, :only => [:edit, :update, :destroy]
  
  #prevent user editing product owned by other user
  def check_self_owner
    if Integer(Product.find(params[:id]).user_id) != session[:user_id]
      flash[:session] = "You are not authorized"
      redirect_to root_path
    end
  end
  
  # GET /products
  # GET /products.json
  def index
    if params[:f].nil?
      @products = Product.all
    else
      flash[:filter] = "All product with #{Category.find(params[:f]).name} category"
      parent = Category.find(params[:f])
      child_select = ""
      if parent.parent_id.nil?
        Category.where("parent_id = #{parent.id}").select("id").each do |child|
          child_select += " or category_id = #{child.id}"
        end
      end
      @products = Product.where("category_id = " + params[:f] + child_select)
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @products }
    end
  end
  
  #show only product owned by user
  def my_products
    @products = Product.find_all_by_user_id(session[:user_id])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @products }
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/new
  # GET /products/new.json
  def new
    @product = Product.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/1/edit
  def edit
    @product = Product.find(params[:id])
  end

  # POST /products
  # POST /products.json
  def create
    params[:product][:user_id] = session[:user_id]
    @product = Product.new(params[:product])

    respond_to do |format|
      if @product.save
        format.html { redirect_to my_products_path, notice: 'Product was successfully created.'}
        format.json { render json: @product, status: :created, location: @product }
      else
        format.html { render action: "new" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
    params[:product][:user_id] = session[:user_id]
    @product = Product.find(params[:id])

    respond_to do |format|
      if @product.update_attributes(params[:product])
        format.html { redirect_to my_products_path, notice: 'Product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    respond_to do |format|
      format.html { redirect_to my_products_url }
      format.json { head :no_content }
    end
  end
end
