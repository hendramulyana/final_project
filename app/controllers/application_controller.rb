class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user
  
  #check is user already login
  def require_login
    if current_user.nil?
      flash[:session] = "Forbidden Access, please login first"
      redirect_to root_path
    else
      return current_user
    end
  end

  private
  #get current user and check is current user is still exist in database
  def current_user
    begin
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    rescue => e
    end
  end
end
