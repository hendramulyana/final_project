class HomePagesController < ApplicationController
  #get root
  def index
    @articles = Article.order("created_at ASC").limit(3)
    @products = Product.order("created_at ASC").limit(6)
  end
end
