class CommentsController < ApplicationController
  before_filter :require_login, :on => :create
  
  #add new comment to article with id of
  def create
    @article = Article.find(params[:comment][:article_id]);
    @comments = @article.comments
    @comment = @article.comments.new(params[:comment])
    @comment.user_id = session[:user_id]

    respond_to do |format|
      if @comment.save
        flash[:notice] = "Add new comment success"
        format.html { redirect_to(article_path(@article), :notice => 'Add new comment success') }
        format.js
      elsif
        format.html { redirect_to(article_path(@article), :notice => 'Add new comment failed')}
        flash[:notice] = "Add new comment failed"
      end
    end
  end
end
