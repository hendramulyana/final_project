class UserMailer < ActionMailer::Base
  default from: "register@onlinestore.com"
  
  def registration_confirmation(user)
    @user = user
    mail(:to => user.email, :subject => "Thank you for registering")
  end
end
