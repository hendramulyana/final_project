class Product < ActiveRecord::Base
  attr_accessible :category_id, :description, :name, :price, :user_id, :weight
  
  validates :category_id, :presence => true
  validates :description, :presence => true,
                          :length => {:minimum => 5,:maximum => 1024}
  validates :name, :presence => true
  validates :price, :presence => true,
                    :numericality => true
  validates :user_id, :presence => true
  validates :weight, :presence => true,
                     :numericality => true
  
  belongs_to :user
  belongs_to :category
end
