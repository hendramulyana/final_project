class User < ActiveRecord::Base
  before_save :encrypt_password
  
  attr_accessor :password, :password_confirmation
  attr_accessible :address, :birthday, :blog, :email,
                  :facebook, :gender, :name, :password_hash,
                  :password_salt, :phone_number, :password, :password_confirmation

  has_many :products, :dependent => :destroy
  has_many :articles, :dependent => :destroy 
  has_many :comments, :dependent => :destroy 
  
  #validation
  validates :password, :presence => {:on => :create},
                       :confirmation => true,
                       :length => {:minimum => 4,:maximum => 32},
                       :on => :create
                       
  validates :email,   :presence => true,
                      :uniqueness => true,
                      :length => {:minimum => 5,:maximum => 255},
                      :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
            
  #encrypt password before save          
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
  
  #check is user already registered
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  #check is instance user is admin
  def is_admin?
    if self.email == "admin@admin.com"
      true
    else
      false
    end
  end
  
end
