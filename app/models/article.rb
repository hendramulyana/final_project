class Article < ActiveRecord::Base
  attr_accessible :body, :title, :user_id
  
  validates :body, :presence => true,
                   :length => {:minimum => 5, :maximum => 2048}
  validates :title, :presence => true,
                    :length => {:maximum => 255}
  validates :user_id, :presence => true
  
  belongs_to :user
  has_many :comments
end
