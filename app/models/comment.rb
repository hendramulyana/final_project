class Comment < ActiveRecord::Base
  attr_accessible :article_id, :user_id, :comment
  
  validates :comment, :presence => true,
                      :length => {:maximum => 100, :minimum => 1}
  validates :article_id, :presence => true
  validates :user_id, :presence => true
  
  belongs_to :user
  belongs_to :article
end
