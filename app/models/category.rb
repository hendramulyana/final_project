class Category < ActiveRecord::Base
  attr_accessible :name, :parent_id
  
  has_many :products
  
  validates :name, :presence => true,
                   :length => {:maximum => 255},
                   :uniqueness => true
                  
  
  #get parent_name of category
  def parent_name
    result=""
    begin
      result = Category.find(self.parent_id).name
    rescue
    end
    return result
  end
  
  #get all child category with parent_id of
  def self.get_child(parent_id)
    return Category.where("parent_id = #{parent_id}")
  end
  
  #get all parent category
  def self.get_parent
    return Category.where("parent_id IS NULL")
  end
end
